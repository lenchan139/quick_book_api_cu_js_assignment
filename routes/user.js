var express = require('express');
var router = express.Router();
let auth = require('basic-auth')
var dbController = require('./modules/mongooseController.js')
var bookObj = dbController.getBasebook
var userObj = dbController.getBaseUser
var commentObj = dbController.getBaseComment
/* GET home page. */

router.get('/:id', function(req, res){
  var id = req.params.id
  let userAuth = auth(req)
  console.log(req.body)
  if (userAuth != undefined ) {
    userObj.find({username:userAuth['name'], password: userAuth['pass']},function(err, result){
      if(err){
        res.send(err)
      }else if(result.length <= 0){

            res.statusCode = 401
            //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
            let jsonEnd = {
              error: "Unauthorized",
              message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
            }
            res.send(jsonEnd)
      }else{
  if(id){
    userObj.findById(id, function(err, result) {
      if(err){
        res.status(404).send(err)
      }else{
          result.password = ""
          res.send(result)
      }

    })
  }else{
    res.status(400).send({
      name:"invaildIdError",
      message: "uid is invaild or null"
    })
  }
}
})
}
})
router.get('/username/:id', function(req, res){
  var id = req.params.id
  let userAuth = auth(req)
  console.log(req.body)
  if (userAuth != undefined ) {
    userObj.find({username:userAuth['name'], password: userAuth['pass']},function(err, result){
      if(err){
        res.send(err)
      }else if(result.length <= 0){

            res.statusCode = 401
            //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
            let jsonEnd = {
              error: "Unauthorized",
              message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
            }
            res.send(jsonEnd)
      }else{
  if(id){
    userObj.findOne({username:id}, function(err, result) {
      if(err){
        res.sttus(404).send(err)
      }else{
          result.password = ""
          res.status(400).send(result)
      }

    })
  }else{
    res.status(400).send({
      name:"invaildIdError",
      message: "username is invaild or null"
    })
  }
}
})
}
})
router.post('/', function(req, res) {
  var username = req.body.username
  var password = req.body.password
  var email = req.body.email
  var nickname = req.body.nickname
  var group = req.body.group
  if(username && password && email && nickname){
    userObj.find({username:username}, function(err,result){
      if(err){
        res.status(400).send(err)
      }else{
        if(result.length >= 1){
          var errObj = {
            name: "userExistError",
            message: "user is exist."
          }
        }else{
          var newObj = new userObj({
            username : username,
            password: password,
            email: email,
            nickname : nickname,
            group:group
          })
          newObj.save(function(err,result){
            if(err){
              res.status(400).send(err)
            }else{
              res.send(result)
            }
          })
        }
      }
    })
  }else{
    var errObj = {
      name: "paramMissingError",
      message: "param missing. please make sure include username, password, email, nickname, group"
    }
    res.status(400).send(errObj)
  }
})

module.exports = router;
