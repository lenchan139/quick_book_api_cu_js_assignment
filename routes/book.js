var express = require('express');
var router = express.Router();
const auth = require('basic-auth')
var dbController = require('./modules/mongooseController.js')
var bookObj = dbController.getBasebook
var userObj = dbController.getBaseUser
var commentObj = dbController.getBaseComment
/* GET home page. */
var authCall = function(res){

      res.statusCode = 401
      //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
      let jsonEnd = {
        error: "Unauthorized",
        message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
      }
      res.send(jsonEnd)
}

router.get('/', function(req, res) {

    bookObj.find({}, (err, result) => {
      if(err){
        res.status(404).send(err)
      }else{
        res.send(result)
      }
    });

})
router.get('/:id', function(req, res) {
  var id = req.params.id
  if(id){
    bookObj.findById(id, (err, result) => {
      if(err){
        res.status(404).send(err)
      }else{
        res.send(result)
      }
    });
  }else{
    var errResult = {
      name : "IdEmptyError",
      message : "the id is empty or not vaild.",
      request_id : id
    }
    res.status(400).send(errResult)
  }
})
router.post('/', function(req, res){

  var title = req.body.title
  var author = req.body.author
  var page = parseInt(req.body.page)
  var isbn = req.body.isbn
  var price = parseInt(req.body.price)
  var descr = req.body.descr
  let userAuth = auth(req)
  console.log(req.body)
  if (userAuth != undefined ) {
    userObj.find({username:userAuth['name'], password: userAuth['pass']},function(err, result){
      if(err){
        res.send(err)
      }else if(result.length <= 0){

            res.statusCode = 401
            //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
            let jsonEnd = {
              error: "Unauthorized",
              message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
            }
            res.send(jsonEnd)
      }else{
  if(title && author && page  && isbn && price  && descr){
    var newBook = new bookObj({
      title : title,
      author:author,
      page:page,
      isbn:isbn,
      price:price,
      descr:descr
    })

    console.log('in')
    newBook.save(function(err,result){
      if(err){
        res.send(err)
      }else{
        res.send(result)
      }
    })
  }else{
    var errObj = {
      name: "paramMissingError",
      message: "param missing. please make sure include title, author, page, isbn, price, descr"
    }
    res.status(404).send(errObj)
  }
}
})
}
})
router.put('/:id',function(req, res){
  var id = req.params.id
  var title = req.body.title
  var author = req.body.author
  var page = parseInt(req.body.page)
  var isbn = req.body.isbn
  var price = parseInt(req.body.price)
  var descr = req.body.descr
  let userAuth = auth(req)
  console.log("UserAuth: " + userAuth)
  if (userAuth != undefined ) {
    userObj.find({username:userAuth['name'], password: userAuth['pass']},function(err, result){
      if(err){
        res.send(err)
      }else if(result.length <= 0){

            res.statusCode = 401
            //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
            let jsonEnd = {
              error: "Unauthorized",
              message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
            }
            res.send(jsonEnd)
      }else{
        if(title || author || page >= 1 || isbn || price || descr){
          bookObj.findOne({_id:id}, function(err, resultById) {

                  console.log(resultById)
            if(err){
              res.send(err)
            }else{
              if(title){resultById.title = title}
              if(author){resultById.author = author}
              if(page >= 1){resultById.page = page}
              if(isbn){resultById.isbn = isbn}
              if(price){resultById.price = price}
              if(descr){resultById.descr = descr}
              resultById.save(function(err,result){

                if(err){
                  res.send(err)
                }else{
                  if(result){
                    res.send(result)
                  }else{
                    res.status(404).send({
                      name: "ResultEmptyException",
                      message: "check your id is corrent"
                    })
                  }
                }
              })
            }
          });
        }else{
          var errObj = {
            name: "paramMissingError",
            message: "param missing. please make sure atleast have one paramater includeds: title, author, page, isbn, price, descr"
          }
          res.status(405).send(errObj)
        }
      }
    })

  }else{
      res.statusCode = 401
      //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
      let jsonEnd = {
        error: "Unauthorized",
        message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
      }
      res.send(jsonEnd)}

})
router.delete('/:id',function(req,res){
  var id = req.params.id

  let userAuth = auth(req)
  if (userAuth != undefined ) {
    userObj.find({username:userAuth['name'], password: userAuth['pass']},function(err, result){
      if(err){
        res.send(err)
      }else if(result.length <= 0){

            res.statusCode = 401
            //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
            let jsonEnd = {
              error: "Unauthorized",
              message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
            }
            res.send(jsonEnd)
      }else{
  bookObj.remove({_id: id}, function(err){
    if(err){
      res.status(404).send(err)
    }else{
      var r = {
        remove_book_id : id,
        status : "success"
      }
      res.send(r);
    }
  })
}
})

}
})
module.exports = router;
