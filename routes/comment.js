var express = require('express');
var router = express.Router();
let auth = require('basic-auth')
var dbController = require('./modules/mongooseController.js')
var bookObj = dbController.getBasebook
var userObj = dbController.getBaseUser
var commentObj = dbController.getBaseComment
/* GET home page. */

router.get('/:id', function(req, res) {
  var id = req.params.id
  if(id){
    commentObj.findById(id, (err, result) => {
      if(err){
        res.send(err)
      }else{
        res.send(result)
      }
    });
  }else{
    var errResult = {
      name : "IdEmptyError",
      message : "the id is empty or not vaild.",
      request_id : id
    }
    res.status(404).send(errResult)
  }
})
router.get('/book/:id',function(req,res){
  var id = req.params.id
  if(id){
    commentObj.find({book_id:id},function(err,result){
      if(err){
        res.status(404).send(err)
      }else{
        if(result.length <= 0){
          res.status(404).send({
            name:"zeroLengthError",
            message:"return nothing,make sure the book id have comment"
          })
        }else{
          res.status(404).send(result)
        }
      }
    })
  }else{
    var errResult = {
      name : "IdEmptyError",
      message : "the id is empty or not vaild.",
      request_id : id
    }
    res.status(400).send(errResult)
  }
})
router.get('/user/:id',function(req,res){
  var id = req.params.id
  if(id){
    commentObj.find({name:id},function(err,result){
      if(err){
        res.status(404).send(err)
      }else{
        if(result.length <= 0){
          res.status(404).send({
            name:"zeroLengthError",
            message:"return nothing,make sure the user id have comment"
          })
        }else{
          res.status(400).send(result)
        }
      }
    })
  }else{
    var errResult = {
      name : "IdEmptyError",
      message : "the id is empty or not vaild.",
      request_id : id
    }
    res.status(400).send(errResult)
  }
})
router.post('/', function(req, res){
  var name = req.body.name
  var book_id = req.body.book_id
  var title = req.body.title
  var content = req.body.content
  if(name && book_id && title && content){
    var newComment = new commentObj({
      name:name,
      book_id:book_id,
      title:title,
      content:content
    })

    console.log('in')
    newComment.save(function(err,result){
      if(err){
        res.status(400).send(err)
      }else{
        res.send(result)
      }
    })
  }else{
    var errObj = {
      name: "paramMissingError",
      message: "param missing. please make sure include name, book_id, title, content"
    }
    res.status(400).send(errObj)
  }
})
router.put('/:id',function(req, res){
  var id = req.params.id
  var name = req.body.name
  var book_id = req.body.book_id
  var title = req.body.title
  var content = req.body.content

        console.log(id)
        let userAuth = auth(req)
        console.log(req.body)
        if (userAuth != undefined ) {
          userObj.find({username:userAuth['name'], password: userAuth['pass']},function(err, result){
            if(err){
              res.send(err)
            }else if(result.length <= 0){

                  res.statusCode = 401
                  //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
                  let jsonEnd = {
                    error: "Unauthorized",
                    message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
                  }
                  res.send(jsonEnd)
            }else{
  if(name || book_id || title || content){
    commentObj.findOne({_id:id}, function(err, resultById) {

            console.log(resultById)
      if(err){
        req.status(404).send(err)
      }else{
        if(name){resultById.name = name}
        if(book_id){resultById.book_id = book_id}
        if(title){resultById.title = title}
        if(content){resultById.content = content}
        resultById.save(function(err,result){

          if(err){
            res.status(400).send(err)
          }else{
            if(result){
              res.send(result)
            }else{
              res.status(404).send({
                name: "ResultEmptyException",
                message: "check your id is corrent"
              })
            }
          }
        })
      }
    });
  }else{
    var errObj = {
      name: "paramMissingError",
      message: "param missing. please make sure atleast have one paramater includeds: name, book_id, title, content"
    }
    res.status(402).send(errObj)
  }
}
})
}
})
router.delete('/:id',function(req,res){
  var id = req.params.id
  let userAuth = auth(req)
  console.log(req.body)
  if (userAuth != undefined ) {
    userObj.find({username:userAuth['name'], password: userAuth['pass']},function(err, result){
      if(err){
        res.send(err)
      }else if(result.length <= 0){

            res.statusCode = 401
            //res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
            let jsonEnd = {
              error: "Unauthorized",
              message: "Unauthorized user. Please check your username and password with BasicAuth to call this API."
            }
            res.send(jsonEnd)
      }else{
  commentObj.remove({_id: id}, function(err){
    if(err){
      res.status(404).send(err)
    }else{
      var r = {
        remove_comment_id : id,
        status : "success"
      }
      res.send(r);
    }
  })
}
})
}
})
module.exports = router;
