var mongoose = require('mongoose')
var dbuser = "qbookAdmin"
var dbpassword = "12345678"
mongoose.connect('mongodb://' + dbuser + ':' + dbpassword + '@ds147659.mlab.com:47659/quick_books')
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log('db connect scuess!')
})
exports.mongoose = mongoose
exports.db = db
exports.getBasebook = mongoose.models.book ||
                        mongoose.model('book',
                        mongoose.Schema({
                          title: String,
                          author: String,
                          page: Number,
                          isbn: String,
                          price: Number,
                          descr: String
                        }))
exports.getBaseUser = mongoose.models.user ||
                      mongoose.model('user',
                      mongoose.Schema({
                        username:String,
                        password:String,
                        email:String,
                        nickname:String,
                        group: String
                      }))
exports.getBaseComment = mongoose.models.comment ||
                          mongoose.model('comment',
                          mongoose.Schema({
                            name:String,
                            book_id:String,
                            title:String,
                            content:String
                          }))
